# Does not work with jetson-tx1-drivers-32.7
>x11-base/xorg-drivers-21
>x11-base/xorg-server-21

#Mask newer libv4l/v4l-utils as patches will need ported forward
>media-libs/libv4l-1.18.1
>media-tv/v4l-utils-1.18.1

# Use version in overlay
>media-video/ffmpeg-4.4.9999

# Downgrade because of old linux headers from l4t kernel
>dev-libs/ell-0.30-r1

#mesa
>media-libs/mesa-21.3.8

#Audio Stuff
>media-libs/alsa-lib-1.2.4
>media-libs/alsa-topology-conf-1.2.4
>media-libs/alsa-ucm-conf-1.2.4
>media-plugins/alsa-plugins-1.2.4
>media-sound/alsa-utils-1.2.4

# Since no overlay masks are supported in profile, the whole package is masked.
# The replaced versions in this overlay are unmasked by package.unmask
dev-qt/qtcore
net-wireless/bluez
sys-apps/shadow
sys-apps/systemd-utils
x11-libs/libdrm

# 2022-11-28 This version does not launch. User stay on console without any error
=x11-misc/lightdm-1.32.0-r1

#Weston
>dev-libs/weston-10.1.0

#libpcap
>net-libs/libpcap-1.9.1-r3
